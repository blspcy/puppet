class rss_samba_server {



file { "/etc/samba/smb.conf":
	ensure => 'present',
	source => "/etc/puppet/files/rss_samba_server/smb.conf",
	owner => 'root',
	mode => '600',
}

}
