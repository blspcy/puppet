class samba { 

file { "/etc/samba/smb.conf":
	ensure => 'present',
	source => '/etc/puppet/files/samba/smb.conf',
	owner => 'root',
	group => 'root',
	mode => 600,
}

}
