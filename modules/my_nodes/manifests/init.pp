class my_nodes {

node 'r04blspcy.managed.mst.edu' { 
include ubuntu
}

node 'r08blspcy.managed.mst.edu' {
include ubuntu
include samba
include puppet_master

}

node 'r10itrss.managed.mst.edu' {
include ubuntu
include rss_samba_server
}
}

