class ubuntu {

package { 'wget': ensure => 'present' }
package { 'git':  ensure => 'latest' }
package { 'nfs-common':  ensure => 'latest' }
package { 'samba': ensure => 'latest' }
package { 'rsync': ensure => 'latest' }
package { 'puppet': ensure => 'latest' }


file { "/etc/cron.hourly/puppet":
	ensure => 'present',
	source => "/etc/puppet/files/ubuntu/puppet",
	owner => 'root',
	group => 'root',
	mode => 600
}


}
